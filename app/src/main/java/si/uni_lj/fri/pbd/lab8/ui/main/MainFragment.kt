package si.uni_lj.fri.pbd.lab8.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import si.uni_lj.fri.pbd.lab8.Product
import si.uni_lj.fri.pbd.lab8.R
import si.uni_lj.fri.pbd.lab8.databinding.MainFragmentBinding
import java.util.*


class MainFragment : Fragment() {

    private var _binding: MainFragmentBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private var mViewModel: MainViewModel? = null
    private var adapter: ProductListAdapter? = null
    private var productId: TextView? = null
    private var productName: EditText? = null
    private var productQuantity: EditText? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        productId = binding.productID
        productName = binding.productName
        productQuantity = binding.productQuantity
        listenerSetup()
        observerSetup()
        recyclerSetup()
    }

    private fun listenerSetup() {
        val addButton = binding.addButton
        val findButton = binding.findButton
        val deleteButton = binding.deleteButton

        addButton.setOnClickListener {
            val name = productName?.text.toString()
            val quantity = productQuantity?.text.toString()
            if (name != "" && quantity != "") {
                // TODO: create a new Product with name and quantity,
                //  insert it in mViewModel and call clearFields()
            } else {
                productId?.text = "Incomplete information"
            }
        }
        findButton.setOnClickListener {
        // TODO: call mViewModel.findProduct with the name a user set in productName

        }
        deleteButton.setOnClickListener {
        // TODO: call mViewModel.deleteProducts with the name a user set in productName and then call clearFields()

        }
    }

    private fun observerSetup() {

        // TODO: set observer for mViewModel.allProducts


        // TODO: set observer for mViewModel.searchResults

    }

    private fun recyclerSetup() {
        adapter = ProductListAdapter(R.layout.product_list_item)
        val recyclerView: RecyclerView = binding.productRecycler
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = adapter
    }

    private fun clearFields() {
        productId?.text = ""
        productName?.setText("")
        productQuantity?.setText("")
    }

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }
}
