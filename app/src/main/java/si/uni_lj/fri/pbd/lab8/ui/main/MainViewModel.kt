package si.uni_lj.fri.pbd.lab8.ui.main

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import si.uni_lj.fri.pbd.lab8.Product
import si.uni_lj.fri.pbd.lab8.ProductRepository


class MainViewModel(application: Application?) : AndroidViewModel(application!!) {
    // TODO: add LiveData fields (allProducts and searchResults)

    // TODO: add ProductRepository field


    // TODO: add functions for inserting, deleting, and finding a product


    init {

        // TODO: set repository, allProducts, and searchResults

    }
}