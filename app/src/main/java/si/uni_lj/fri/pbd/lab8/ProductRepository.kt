package si.uni_lj.fri.pbd.lab8

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData


class ProductRepository(application: Application?) {
    val searchResults = MutableLiveData<List<Product>>()
    val allProducts: LiveData<List<Product>>


    // TODO: Add DAO reference

    fun insertProduct(newproduct: Product) {
        // TODO: run query to insert a product on the executor

    }

    fun deleteProduct(name: String) {
        // TODO: run query to delete a product on the executor

    }

    fun findProduct(name: String) {
        // TODO: run query on the executor, postValue to searchResults

    }


    init {
        // TODO: Add the code in the init block
    }
}